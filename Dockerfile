#Dockerfile
ARG AB_FIXED_RELEASE=21.2.85-centos7
FROM atlas/analysisbase:$AB_FIXED_RELEASE

#WORKDIR /usr/src/app
USER root

COPY skim_and_slim/ .
COPY install_python_deps.sh install_python_deps.sh

RUN source /home/atlas/release_setup.sh && \
    bash install_python_deps.sh && \
		rm install_python_deps.sh

