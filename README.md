# MBJ_utils

This is the repository that was created to collect scripts that are used for the ATLAS SUSY analysis with multiple b-jets in the final state for the purpose od RECAST implementation. These scripts are utilities that are used as an intermediate steps in the analysis and were saved only locally. Authors of scripts are listed bellow:

- skim_and_slim : [Michael Donald Hank](https://gitlab.cern.ch/mhank)



