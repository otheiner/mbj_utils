#!/usr/bin/env bash

set -e

pip install --upgrade pip
pip install --upgrade --no-cache-dir scipy
pip install --no-cache-dir root-numpy
pip install pandas
