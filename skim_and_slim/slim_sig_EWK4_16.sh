#!/bin/bash

for file in $1/*root; do
    did=`echo $file | awk -F'[.]' '{print $2}' | sort -u`
    echo $did
    num=`echo $file | cut -f14 -d"/" | cut -f4 -d"."`
    echo $num
    mkdir -p ./$did
    python slim_sig.py -y $2 -d $file -o ./$did -p 16a_"$did"_"$num"
done

